import React from 'react';


class List extends React.Component {
    
    state = {
        dragon: []
    };

    componentDidMount() {
        fetch('http://5c4b2a47aa8ee500142b4887.mockapi.io/api/v1/dragon')
            .then(res => res.json())
            .then(res => {
                this.setState({
                    dragon: res
                });
            });
    }
    

    render() {

        return (
            <div class="grid-container">
                <div></div>
                <div>
                <h1 id="fonteTitulo"> Lista de Dragoes </h1>
                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>Data de Criação</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Histórias</th>
                        <th>Imagem</th>
                        
                    </tr>
                    {this.state.dragon.map(item => (
                        <tr>
                            <td>{item.id}</td>
                            <td>{item.createdAt}</td>
                            <td>{item.name}</td>
                            <td>{item.type}</td>
                            <td>{item.histories}</td>
                            <td>{item.imageUrl}</td>
                        </tr>
                    ))}
                </table>
            </div>
            </div>
        );

    }
}

export default List;