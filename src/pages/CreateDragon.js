import React from 'react';
import axios from 'axios';

export default class CreateDragon extends React.Component {
  state = {
    dataCriacao: '',
    nome: '',
    tipo: '',
    historias: '',
    imagem: '',
  }

  handleChange = event => {
    this.setState({ dataCriacao: event.target.value });
    this.setState({ nome: event.target.value });
    this.setState({ tipo: event.target.value });
    this.setState({ historias: event.target.value });
    this.setState({ imagem: event.target.value });
  }


  handleSubmit = event => {
    event.preventDefault();

    const dragao = {
      dataCriacao: this.state.dataCriacao,
      nome: this.state.nome,
      tipo: this.state.tipo,
      historias: this.state.historias,
      imagem: this.state.imagem
    };

    axios.post(`http://5c4b2a47aa8ee500142b4887.mockapi.io/api/v1/dragon`, { dragao })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Person Name:
            <input type="text" name="dataCriacao" onChange={this.handleChange} />
            <input type="text" name="nome" onChange={this.handleChange} />
            <input type="text" name="tipo" onChange={this.handleChange} />
            <input type="text" name="historias" onChange={this.handleChange} />
            <input type="text" name="imagem" onChange={this.handleChange} />
          </label>
          <button type="submit">Add</button>
        </form>
      </div>
    )
  }
}