import React, { useState } from "react"
import './App.css';
import About from './pages/About';
import PrivateRoute from './PrivateRoute';
import List from './pages/List';
import updateDragon from './pages/UpdateDragon';
import createDragon from './pages/CreateDragon';
import Login from './pages/Login';
import Signup from './pages/Signup';
import { AuthContext } from "./context/auth";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <div className="App" >
        <nav>
          <a href="/">Home</a>
          <a href="/about">About</a>
          <a href="/login">Login</a>
          <a href="/signup"> SignUp</a>
          <a href="/list"> List</a>
          <a href="/updateDragon"> Update</a>
          <a href="/createDragon"> Create</a>
          </nav>
          <Switch>

            <PrivateRoute path="/about" component={About} />
            <Route path="/login" component={Login} />
            <PrivateRoute path="/signup" component={Signup} />
            <PrivateRoute path="/list" component={List} />
            <PrivateRoute path="/updateDragon" component={updateDragon} />
            <PrivateRoute path="/createDragon" component={createDragon} />
            {/* <PrivateRoute path="/list" component={List} /> */}
          </Switch>
        </div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
